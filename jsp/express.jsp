<html>
<head>
<title>Expression</title>
</head>
<body>

<% out.println("The name  is "); %>
<% String name="Tinagaran"; %>
<% String color="blue"; %>
<%=name %> <%-- place the result without printing --%>
<br>

<% out.println("The expression number is "); %>
<% int i=10, j=20, k=30; %>
<%= i+j+k %>
<br>
<font size="<%= k%>" color="<%=color%>">

SAMPLE
</font>
</body>
</html>
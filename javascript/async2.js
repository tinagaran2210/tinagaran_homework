console.log("async...")

function saySomething(x){
    return new Promise(resolve =>{
        setTimeout(()=> {
            resolve("the word is "+x);

        },200)
    });
}

async function talk(x){
    const words = await saySomething(x)
    console.log(words)
}

function saySomething2(x, timer){
    return new new Promise(resolve =>{
        setTimeout(()=> {
            resolve("the word is "+x);

        },timer) //in ms
    });
}

async function talk2(x, timer){
    const words = await saySomething2(x, timer)
    console.log(words)
}

talk("hi")
talk2(2, 1000)
talk2(3, 2000)
talk2(4, 40)
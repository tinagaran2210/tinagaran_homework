console.log("Arrays file....")
arr3 = new Array(10)
arr3
arr2 = ["black","white","red"]
arr2.push("orange")// inserts
arr2
arr1 = [1,2,3,4]
arr1
arr=['whats','eheh',1]
let newarr = arr.concat(arr1)
newarr
let lastel=arr1.pop() //deletes last number
arr1
lastel
arr2.splice(3,0,"brown")
arr2
arr1.splice(0,2) //deletes index
arr1

//find
arr8 = [2,6,7,6,8,6]
let findValue = arr8.find(function(e){return e==6})
findValue
findValue = arr8.find(function(e){return e%2==0}) //finds even number
findValue
let findValue2 = arr8.find(e => e === 8) //finds specific
findValue2
console.log(arr8.indexOf(6)) //first occurance of 6 from index 0 is
console.log(arr8.indexOf(6,3)) //first occurance of 6 from index 3 is
console.log(arr8.indexOf(6,-1)) //first occurance of 6 from index -1 is
console.log(arr8.indexOf(2,2)) //doesnt exist
arr2.sort() //sorts it
arr2
arr8.reverse() //reverse
arr8

//multidimensional
let someval1 = [1,2,3]
let someval2 = [4,5,6]
let someval3 = [7,8,9]
let arrOfArr = [someval1, someval2, someval3]
arrOfArr

//expression ? statement for true : statment for false
let age =15
let access = age < 18 ? "denied": "allowed";
access

age < 18 ? console.log("DENIED"): console.log("ALLOWED")

// for (initialize variable; contion; statement){
    //code
//}

for(let i = 0; i<10; i++)
{
    console.log(i)
}

let arrloop = []
for (let i =0; i<10; i=i+2)
{
    arrloop.push(i);
}

console.log(arrloop)

for (let i =0; i< arr.length; i++)
{
    console.log(arr[i]) 
}

for (let el of arr)
{
    console.log(el)
}

console.log("----------------------")
arr.forEach(e=> console.log(e))

//spread operators

let spread = ["hi","how","are","you"]
let message = ["greetings",...spread,"!!!"]
message 

function addTwoNumbers(x,y){
    console.log(x+y)

}
let numarr = [2,3]

addTwoNumbers(...numarr)
addTwoNumbers(2,3)

function addFourNums(x,y,z,w)
{
    console.log(x+y+z+w)
}
let numarr2 = [4,5]

//Syntactic Sugar
addFourNums(...numarr,...numarr2)
addTwoNumbers(2,3,4,5)

farray = ["one","two"]

//filter by variable 
newarr
function checkString (element, index){
    return typeof element ==="string"
}

let filterArr = newarr.filter(checkString)
console.log(filterArr)
console.log(newarr.every(checkString))
console.log(farray.every(checkString))

let numarray = [1,2,3,4]
let mapped_arr = numarray.map(x=>x+1); //mapped means same number, here plus 1 equation after maping
mapped_arr

//custom sort methods
const students = [
    {firstname: "Tinagaran", lastname:"Raj", age: 24},
    {firstname: "Adalina", lastname:"Kadir", age: 24},
    {firstname: "Hairul", lastname:"Wafiq", age: 23},
    {firstname: "Li Ren", lastname:"Ng", age: 22},
];

students.sort((a,b)=> a.age - b.age)
//or
students.sort(function(a,b)
{
    return a.age -b.age;
})
//or
students.sort(function(a,b)
{
    if(a.age<b.age)
    {
        return -1;
    }
    else if(a.age >b.age)
    {
        return 1;
    }
    else {
        return 0;
    }
})

console.log(students)

//sort last name
console.log("aba".localeCompare("zaaa"))
students.sort((a,b) => a.lastname.localeCompare(b.lastname))
console.log(students)


//flat array
const threeDArray = [1,[2,[3,4,5],6],7]
console.log(threeDArray[1][1][1])
const flat2d = threeDArray.flat(1)
flat2d
const flat1d = threeDArray.flat(2)
flat1d

//remove duplicates items from an array
const numduplicate = [1,3,42,1,5,6,5,42,9]
//see whatsapp with newarr


const panels = document.querySelectorAll('.panel') //grab panels from html

panels.forEach( panel => {
    panel.addEventListener('click', ()=> {
        //remove active class from others
        removeActiveClasses()
        panel.classList.add('active')
    })
})

function removeActiveClasses(){
    panels.forEach(panel => {
        panel.classList.remove('active')
    })
}
console.log("Functions...")

function addNums(x=0,y=0){
    return x+2*y
}

console.log(addNums(4,5))
console.log(addNums())
console.log(addNums(2,3))

//(param1, param2) ==> body of the function

function doStuff(x){
    console.log(x)
}

doStuff("hi")

let doStufdArrow = x => console.log(x)
doStufdArrow("Arrow")

let addTwoNumsAgain = (x,y) => console.log(x+y)
addTwoNumsAgain(5,6)

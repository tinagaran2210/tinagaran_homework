arr1 = ["Milk","Bread","Apple"]
arr1

arr1[1]="Banana"
arr1.splice(2,0,"Egg")
arr1

arr1.pop()
arr1

arr1.sort()
arr1

arr1.splice(1,0,"Carrot","Lettuce")
arr1

arr2= ["Juice","Pepsi"]
arr2

let newarr = arr1.concat(arr2,arr2)
newarr

//create an empty array to use as a shopping list
//add milk, bread, apple to  the list
//update bread with banana and egg
//remove the last item from the array
//sort the list alphabetically
//find an output the index value of milk
//after banana, add carrot and lettuce.
//create a new list containing juice and pepsi
//combine both list, adding the new list twice at the end of the first list
//get the last index value of pepsi and output item
//[banana,carrot,lettuce,eggs,milk,juice,pepsi,juice,pepsi]
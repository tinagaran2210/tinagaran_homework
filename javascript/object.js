console.log("Objects")
let arr = [0,1,2]

console.log(typeof arr)
console.log(typeof 1)
console.log(typeof "hi")

let dog = {
    dogName: "Javascript",
    weight: 2.5,
    color: "brown",
    age: 4,
    willBite: true
}

dog 
let dogColor1 = dog ["color"]
dogColor1
let dogColor2 = dog.color
dogColor2 
dog["color"] = "blue"
dog.weight= 2.1
dog
console.log (typeof dog.age)
//can change type of attribute
dog.age = "four"
console.log (typeof dog.age)
dog

property = "color" //can change property to print
console.log(dog[property])
dog.address = {
    roadname: "Jalan Amp",
    housenumber : 432
}
console.log(dog.address.roadname)
console.log(dog["address"].roadname)
console.log(dog["address"]["roadname"])
dog.interests = ["biting","chewing","barking"]
console.log(dog.interests [0])

let dog2 = {
    dogName: "Java",
    weight: 2.5,
    color: "black",
    age: 3,
    willBite: true
}

dogs = [dog,dog2]

console.log(dogs[0].dogName)

dog
for (let el in dog)
{
    console.log(el)
    console.log(dog[el])
    console.log(el +"::"+dog[el])
}

let arrKeys = Object.keys(dog)
arrKeys
for(let key of Object.keys(dog)){
    console.log(key)
}
console.log(Object.values(dog))
for(let key of Object.values(dog)){
    console.log(key)
}
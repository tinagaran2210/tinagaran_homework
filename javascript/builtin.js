console.log("built in functions...")

let s = "Hello"

console.log(
    s.concat(" there!")
    .toUpperCase()
    .replace("THERE","you")
    .concat(" You are amazing!")
)
let x = 7;
console.log(Number.isNaN(x));

let uri = "https://google.abc/submit?name=Tinagaran Subra"
console.log(uri) 
//encoding uri
let encoded_uri = encodeURI(uri)
console.log(encoded_uri)

//decoding uri
let decode_uri = decodeURI(uri)
console.log(decode_uri)

//change the variable by typecasting
str_int = "6"
let int_int = parseInt(str_int)

console.log(typeof str_int)

let str_float = "7.6"
let int_float = parseInt(str_float)
console.log(int_float)

let float_float = parseFloat(str_float)
float_float

//String splitting
let result ="Hello dude,how are you"
let arr_result = result.split(",") //split after the symbol occurs. eg: o, l,
arr_result
//can be used to create array from string
let fruits = "apple,oranges,banana"
let arr_fruits = fruits.split(",")
arr_fruits 

//parsing array
let letters = ["a","b","c","d"]
x = letters.join("||")
x


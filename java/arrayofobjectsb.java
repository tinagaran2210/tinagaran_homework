class Student
{
    int stu_id;
    String stu_name;

    Student(int id, String name)
    {
        stu_id = id;
        stu_name = name;
    }
    void display()
    {
        System.out.println("Student ID: "+stu_id);
        System.out.println("Student Name: "+stu_name);

    }

}

public class arrayofobjectsb
{
    public static void main(String args[])
    {
        Student s[]= new Student [5];
        s[0]=new Student(1111, "Tinagaran");
        s[1]=new Student(2222, "Abu Bakar");
        s[2]=new Student(3333, "Mat Sabu");
        s[3]=new Student(4444, "Along");
        s[4]=new Student(5555, "Morbin");


        for (int i=0;i<s.length;i++)
        {
            s[i].display();
        }
//        s[1].display();
    }
}
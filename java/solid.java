class solid {

    int volume (int a)
    {
        return a*a*a;
    }
    int volume (int a, int b)
    {
        return a*a*b;
    }
    int volume (int a, int b, int c)
    {
        return a*b*c;
    }
   

    public static void main(String[] args) 
    {
        solid s1 = new solid();
        solid s2 = new solid();
        solid s3 = new solid();
        System.out.println("Volume of Cube: "+s1.volume(10));
        System.out.println("Volume of Square Prism: "+s2.volume(10,20));
        System.out.println("Volume of Rectangular Prism: "+s3.volume(30,5,10));

    }
    
}

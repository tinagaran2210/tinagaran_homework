class testoverload {

    static int math(int a, int b)
    {
        return a+b;
    }
    static int math(int a, int b, int c)
    {
        return a*b-c;
    }

    public static void main(String[] args) 
    {
        System.out.println(math(10,20));
        System.out.println(math(10,20,30));
    }
    
}

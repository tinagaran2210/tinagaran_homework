import java.sql.*;
import java.util.Scanner;

class NewJDBC
{
public static void main(String args[])
{
Scanner sc=new Scanner(System.in);
try
{
Class.forName("com.mysql.cj.jdbc.Driver");
Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb","root","");
//here mydb is database name, root is username and password
int ch=0;
while(ch!=5)
{
System.out.println("1.Add Employee Data---2.Display Employee data---3.Add Student Data---4.Display Student data---5.Exit");
ch=sc.nextInt();


if(ch==1)
{
System.out.println("Input Employee ID, Name ,Designation :");
int id=sc.nextInt();
String nam =sc.next();
String design =sc.next();
String sql = "insert into employee " + " (empid, ename, desig)" + " values (?, ?, ?)";
PreparedStatement pstmt = con.prepareStatement(sql);
// set param values
pstmt.setInt(1,id);
pstmt.setString(2,nam);
pstmt.setString(3,design);
// 3. Execute SQL query1
pstmt.executeUpdate();
}


if(ch==2)
{
Statement stmt=con.createStatement();
ResultSet rs=stmt.executeQuery("select * from employee");
while(rs.next())
System.out.println(rs.getInt(1)+" "+rs.getString(2)+" "+rs.getString(3));
}

if(ch==3)
{
        System.out.println("Input Student ID, Name ,Course :");
        int stuid=sc.nextInt();
        String sname =sc.next();
        String course =sc.next();
        String sql2 = "insert into student " + " (stuid, sname, course)" + " values (?, ?, ?)";
        PreparedStatement pstmt2 = con.prepareStatement(sql2);
// set param values
        pstmt2.setInt(1,stuid);
        pstmt2.setString(2,sname);
        pstmt2.setString(3,course);
// 3. Execute SQL query1
        pstmt2.executeUpdate();
}
if(ch==4)
{
    Statement stmt2=con.createStatement();
    ResultSet rs2=stmt2.executeQuery("select * from student");
    while(rs2.next())
    System.out.println(rs2.getInt(1)+" "+rs2.getString(2)+" "+rs2.getString(3));
}
}
sc.close();
con.close();
}
catch(Exception e)
{ System.out.println(e);}
}
}
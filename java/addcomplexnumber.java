//This is the employee class
class Employee
{
    int id;
    double pay = 0.00;
    String status;

    //Default constructor
    //zero argument constructor
    Employee()
    {
        id = 0;
        pay = 0.00;
        status = " ";
    }

    //Parameterized constructor
    Employee(int i, double p)
    {
        id = i;
        pay = p;
    }

    //Entering the employee data
    public void insert (int i, double p)
    {
        id = i;
        pay = p;
    }

    public void display()
    {
        if(pay >= 50)
        {
            status= "High salary";;
        }
        else if(pay > 30 && pay < 50)
        {
            status= "Medium salary";
        }
        else if(pay < 30)
        {
            status= "Low salary";
        }

        System.out.println("ID: "+id+ " Salary: RM"+pay+ " Status: " +status);
    }
    
}

//This is the main class
class Main{
    
    public static void main(String args [])
    {
        Employee emp1 = new Employee();           
        Employee emp2 = new Employee();        
        Employee emp3 = new Employee();        
        Employee emp4 = new Employee();        
        Employee emp5 = new Employee();        

        Employee emp6 = new Employee(2, 40);

        empnew = emp1 + emp2;
        empnew.add();
        empnew.result();

        emp1.insert(1, 20);
        emp2.insert(2, 40);
        emp3.insert(3, 50);
        emp4.insert(4, 70);
        emp5.insert(5, 10);

        // emp1.display();
        // emp2.display();
        // emp3.display();
        // emp4.display();
        // emp5.display();

        emp6.display();
    }
}

import java.util.Scanner; 

class atm {
    public static void main(String args[])
    {
        int balance = 2000;
        int withdraw, deposit;
        Scanner input = new Scanner(System.in);
        while(true)
        {
        System.out.println();
        System.out.println("Welcome to Bank ATM");
        System.out.println("1. Check my balance ");
        System.out.println("2. Withdraw amount ");
        System.out.println("3. Deposit amount ");
        System.out.println("4. Exit: ");
        System.out.print("What do you choose to do? ");
        int choice = input.nextInt();
        System.out.println();

        switch(choice)
        {
            case 1:
            System.out.println("Balance is RM"+balance);
            break;

            case 2:
            System.out.println("How much would you like to withdraw? ");
            withdraw = input.nextInt();
            if(withdraw>balance)
            {
                System.out.println("Balance is insufficient");
            }
            else
            {
                balance=balance-withdraw;
                System.out.println("Withdrawal amount of RM"+withdraw+" is successful ");
                System.out.println("Your new balance is RM"+balance+" is successful ");
            }
            break;

            case 3:
            System.out.println("How much would you like to deposit? ");
            deposit = input.nextInt();
            balance= balance+deposit;
            System.out.println("Deposit amount of RM"+deposit+" is successful ");
            System.out.println("Your new balance is RM"+balance+" is successful ");
            break;

            case 4:
            System.out.println("Thanks for you time!!!");
            System.exit( 0 );
            break;

            default:
            System.out.println("Enter the correct option!!!");
        }

        }
    }
}

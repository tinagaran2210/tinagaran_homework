class loops {
    public static void main(String args[])
    {
        int i;
        System.out.println("For Loop");
        for(i=10;i>0;i--)
        {
            if(i==5)
            {
                break;
            }
            
            System.out.println("i="+i);
        }
        
        System.out.println("While Loop");
        i=0;
        while(i<10)
        {
            if(i==4)
            {
                i++;
                continue;
                
            }     
            System.out.println("i="+i);
            i++;
            
        }

        System.out.println("Do While Loop");
        i=5;
        do
        {
            System.out.println("i="+i);
            i++;
            
        }while(i<20);

        

    
    }

       
}


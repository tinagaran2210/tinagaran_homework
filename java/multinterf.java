interface Printable
{
    void print();
}

interface Showable
{
    void show();
}

abstract class abcls
{
    abstract void prnt();
}

class abclst extends abcls
{
    void prnt()
    {
        System.out.println("abstract method");
    }
}

class multinterf extends abcls implements Printable,Showable{
    public void print()
    {
        System.out.println("Hello");
    }
    public void show()
    {
        System.out.println("Welcome");
    }
    public void prnt()
    {
        System.out.println("abstract method");
    }
    public void childshow()
    {
        System.out.println("child show");
    }

    public static void main(String args[]) {
        multinterf obj = new multinterf();
        abclst x = new abclst();
        obj.print();
        obj.show();
        obj.prnt();
        obj.childshow();
        x.prnt();
    }
}